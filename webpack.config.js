const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

const outputDirectory = path.resolve(__dirname, 'dist');
const isCSS = /\.(s*)css$/;
const isImage = /\.(png|svg|jpg|gif)$/;
const isFont = /\.(woff|woff2|eot|ttf|otf)$/;

module.exports = {
    entry: {
        polyfills: './assets/js/polyfills.js',
        main: ['./assets/js/main.js', './assets/css/main.scss']
    },
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                cache: true,
                parallel: true,
                sourceMap: true
            }),
            new OptimizeCssAssetsPlugin({})
        ]        
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),        
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new MiniCssExtractPlugin({
            filename: '[name].min.css',
            chunkFilename: '[id].css'
        }),
        new CopyPlugin([
            { from: '*.hbs', to: outputDirectory },
            { from: 'package.json', to: outputDirectory },
            { from: './partials', to: outputDirectory + '/partials' },
            { from: './node_modules/bulma-extensions/bulma-carousel/dist/bulma-carousel.min.js', to: outputDirectory }
        ]),
        new ZipPlugin({
            filename: 'ghost-custom-theme.zip',
            pathMapper: function(assetPath) {
                if (assetPath.endsWith('.js'))
                    return path.join(path.dirname(assetPath), 'assets/js', path.basename(assetPath));
                
                if (isCSS.test(assetPath))
                    return path.join(path.dirname(assetPath), 'assets/css', path.basename(assetPath));                

                if (isFont.test(assetPath))
                    return path.join(path.dirname(assetPath), 'assets/fonts', path.basename(assetPath));
                
                if (isImage.test(assetPath))
                    return path.join(path.dirname(assetPath), 'assets/img', path.basename(assetPath));

                return assetPath;
            }
        }),
        new StylelintPlugin({
            configFile: '.stylelintrc',
            context: 'assets',
            files: 'css/*.scss',
            failOnError: false,
            quiet: false
        })
    ],
    output: {
        filename: '[name].min.js',
        path: outputDirectory
    },
    mode: "production",
    module: {
        rules: [
            {
                test: isCSS,
                use:['style-loader', MiniCssExtractPlugin.loader, 'css-loader','sass-loader']
            }, 
            {
                test: isImage,
                use:['file-loader']
            },
            {
                test: isFont,
                use:['file-loader']
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|dist)/,
                use: ['babel-loader', 'eslint-loader']
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js']
    }    
};