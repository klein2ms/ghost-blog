/* global document */
/* global bulmaCarousel */
/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "[iI]gnored" }] */
(() => {
  document.addEventListener('DOMContentLoaded', () => {
    const $navbarBurgers = Array
      .prototype
      .slice
      .call(document.querySelectorAll('.navbar-burger'), 0);

    if ($navbarBurgers.length > 0) {
      $navbarBurgers.forEach(($el) => {
        $el.addEventListener('click', () => {
          const { target } = $el.dataset;
          const $target = document.getElementById(target);
          $el.classList.toggle('is-active');
          $target.classList.toggle('is-active');
        });
      });
    }

    const carouselsIgnored = bulmaCarousel.attach();
  });
})();
